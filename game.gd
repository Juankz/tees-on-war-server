extends Node

var world

# List of players
# player = { id, name, kills, deaths, assis }
var players


# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(world)

master func request_world_creation():
	rpc_id(get_tree().get_rpc_sender_id(), "create_world", "res://Worlds/Level.tscn")

master func request_player_creation(player_info):
	# TODO: Validate player information 
	var Player = load("res://Players/Player.tscn")
	var player = Player.instance()
	player.name = str(player_info.id)
	world.get_node("Players").add_child(player)
	
	players[player_info.id] = player_info
	
	rpc("create_players", players)
	
func remove_player(id):
	world.get_node("Players").get_node(str(id)).queue_free()
	players.erase(id)
	rpc("remove_player",id)
	
func _init():
	world = load('res://Worlds/Level.tscn').instance()
	players = {}
