extends Node

# Default game port
const DEFAULT_PORT = 10567
# Max number of players
const MAX_PLAYERS = 16

# Callback from SceneTree
func _player_connected(id):
	print('Client ' + str(id) + ' connected to Server')

# Callback from SceneTree
func _player_disconnected(id):
	get_tree().root.get_node("Game").remove_player(id)
	print("disconnected ", id)

# Called when the node enters the scene tree for the first time.
func _ready():
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PLAYERS)
	get_tree().set_network_peer(host)
	
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
